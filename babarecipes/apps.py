from django.apps import AppConfig


class BabarecipesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'babarecipes'
