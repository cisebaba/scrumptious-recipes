from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin

from recipes.forms import RatingForm


from recipes.models import Recipe
from recipes.models import ShoppingItem, Ingredient, FoodItem


def log_rating(request, recipe_id):
    if request.method == "POST":
        form = RatingForm(request.POST)
        if form.is_valid():
            rating = form.save(commit=False)
            try:
                rating.recipe = Recipe.objects.get(pk=recipe_id)
            except Recipe.DoesNotExist:
                return redirect("recipes_list")

            rating.save()
    return redirect("recipe_detail", pk=recipe_id)


class RecipeListView(ListView):
    model = Recipe
    template_name = "recipes/list.html"
    paginate_by = 2


class RecipeDetailView(DetailView):
    model = Recipe
    template_name = "recipes/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["rating_form"] = RatingForm()
        # return context

        foods = []

        for item in self.request.user.shopping_items.all():
            # Add the shopping item's food to the list
            foods.append(item.food_item)
        context["servings"] = self.request.GET.get("servings")

        # Put that list into the context
        context["food_in_shopping_list"] = foods

        return context


class RecipeCreateView(LoginRequiredMixin, CreateView):
    model = Recipe
    template_name = "recipes/new.html"
    fields = ["name", "author", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)


class RecipeUpdateView(LoginRequiredMixin, UpdateView):
    model = Recipe
    template_name = "recipes/edit.html"
    fields = ["name", "description", "image", "servings"]
    success_url = reverse_lazy("recipes_list")


class RecipeDeleteView(LoginRequiredMixin, DeleteView):
    model = Recipe
    template_name = "recipes/delete.html"
    success_url = reverse_lazy("recipes_list")


def create_shopping_item(request):
    ingredient_id = request.POST.get("ingredient_id")
    # Get the value for the "ingredient_id" from the
    # request.POST dictionary using the "get" method
    # print(ingredient_id)
    ingredient = Ingredient.objects.get(id=ingredient_id)
    # Get the specific ingredient from the Ingredient model
    # using the code
    # Ingredient.objects.get(id=the value from the dictionary)
    user = request.user
    # Get the current user which is stored in request.user

    try:
        ShoppingItem.objects.create(food_item=ingredient.food, user=user)
        # Create the new shopping item in the database
        # using ShoppingItem.objects.create(
        #   food_item= the food item on the ingredient,
        #   user= the current user
        # )
    except IntegrityError:  # Raised if someone tries to add
        pass  # the same food twice, just ignore it

    # Go back to the recipe page with a redirect
    # to the name of the registered recipe detail
    # path with code like this
    return redirect("recipe_detail", pk=ingredient.recipe.id)
    # return redirect(
    #     name of the registered recipe detail path,
    #     pk=id of the ingredient's recipe
    # )


class ShoppingItemListView(LoginRequiredMixin, ListView):
    model = ShoppingItem
    template_name = "shopping_items/list.html"

    def get_queryset(self):
        return ShoppingItem.objects.filter(user=self.request.user)


def delete_all_shopping_items(request):
    ShoppingItem.objects.filter(user=request.user).delete()
    return redirect("shopping_item_list")
    # Delete all of the shopping items for the user
    # using code like
    # ShoppingItem.objects.filter(user=the current user).delete()

    # Go back to the shopping item list with a redirect
    # to the name of the registered shopping item list
    # path with code like this
    # return redirect(
    #     name of the registered shopping item list path
    # )
